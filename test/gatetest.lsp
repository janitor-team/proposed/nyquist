(defun gate-test ()
 (setf yy (gate xx 1.0 0.1 2.0 0.1 0.5))
 ; (s-plot yy)
 ; Ran this to get the answers below
 ; (dolist (tim times)
 ;   (print (list tim (sref yy tim))))
 ; (s-save (scale 0.9 yy) "gatetest.wav")
 (dolist (ans answers)
   (setf actual (sref yy (first ans)))
   (setf err (abs (- actual (second ans))))
   (cond ((> err (/ 1.0 32767.0)) ; one LSB at 16 bits
          (error "gate-test failed with time/expected/actual"
                 (list (first ans) (second ans) actual)))))
 (print "     -> gate-test PASSED"))

; (defun f2 () (gate-test))
; (defun f3 () (s-plot (scale (/ 1.0 0.9) (s-read "gatetest.wav"))))

(setf times '(0.1 1.0 2.0 2.1 2.9 2.93 2.96 3.0 3.3 3.6 4.0 4.9))
(setf answers '((0.1 0.944061)
                (1 0.334965)
                (2 0.105925)
                (2.1 0.1)
                (2.9 0.1)
                (2.93 0.199526)
                (2.96 0.398107)
                (3 1)
                (3.3 1)
                (3.6 0.891251)
                (4 0.562341)
                (4.9 0.199526)))
 
(set-control-srate 100)
(set-sound-srate 100)

(setf xx (pwl 0 1 0.1 0 3 0 3 1 4 0 5))

(setf zz (pwl 0 1 0.02 0 1.99 0 2.0 1 2.01 0 2.09 0 2.1 1 2.11 0 
              2.99 0 3 1 3.01 0 3.09 0 3.1 1 3.11 0 3.19 0 3.2 1 3.21 0 5))

;(defun noise-gate (snd &optional (lookahead 0.5) (risetime 0.02) (falltime 0.5)
;                                                 (floor 0.01) (threshold 0.01))
;  (let ((rms (lp (mult snd snd) (/ *control-srate* 10.0))))
;    (setf save-rms rms)
;    (setf threshold (* threshold threshold))
;    (mult snd (gate rms lookahead risetime falltime floor threshold))))
    
;(defun ngtest ()
;  (setf xx (mult (stretch 5 (lfo 40)) (pwl 0 1 0.5 0 2 0 2 1 2.5 1 2.5 0 5)))
;  (setf xx (sum xx (scale 0.01 (stretch 5 (noise)))))
;  (setf yy (noise-gate xx))
;  (s-plot (vector xx yy)))

(gate-test)

(set-control-srate 2205.0)
(set-sound-srate 44100.0)
