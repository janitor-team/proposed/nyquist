;; csvwrite.lsp -- csv file writer
;;
;; Roger B. Dannenberg
;; June 2022

;; write a CSV file. See csvread.lsp for details on representation
;;
(defun csv-write (filename csv)
  (let ((outf filename) need-comma)
    (cond ((stringp outf)
           (setf outf (open filename :direction :output))))
    (cond ((not (filep outf))
           (error (format nil "csv-write: could not get a file to write from ~A"
                          filename))))
    (dolist (row csv)
      (setf need-comma nil)
      (dolist (col row)
        (cond (need-comma (princ #\, outf)))
        (cond (col
               (prin1 col outf)))
        (setf need-comma t))
      (princ #\Newline outf))
    (close outf)))

